import org.apache.spark.graphx._
import org.apache.spark.graphx.util.GraphGenerators

val edgeFile = "ex_data/roadnet/roadNet-CA.txt"
// "ex_data/roadnet/roadNet-CA.txt"
val profileFile = "ex_data/roadnet/node.txt"

// Load the edges as a graph
val graph = GraphLoader.edgeListFile(sc, edgeFile)
// Run PageRank
val ranks = graph.pageRank(0.0001).vertices
// Join the ranks with the usernames
val users = sc.textFile(profileFile).map { line =>
  val fields = line.split(",")
  (fields(0).toLong, fields(1))
}
val ranksByUsername = users.join(ranks).map {
  case (id, (username, rank)) => (username, rank)
}

val result = ranksByUsername.sortBy(-_._2);