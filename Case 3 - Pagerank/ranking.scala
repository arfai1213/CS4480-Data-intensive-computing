var lines = sc.textFile("ex_data/roadnet/roadNet-CA.txt");

var rdd = lines.map(x => (x.split("	")(1), x));

var rddGrouped = rdd.groupByKey();

var rddFormated = rddGrouped.map({
	case (desId, resIds) => (desId, resIds.size)	
})

var rddSorted = rddFormated.sortBy(-_._2);

println(rddSorted.collect.take(10).mkString("\n"));