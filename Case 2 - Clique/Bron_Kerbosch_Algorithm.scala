import org.apache.spark._
import org.apache.spark.rdd.RDD

// var lines = sc.textFile("facebook_combined(undirected).txt");

var lines = sc.textFile("twitter_combined(directed).txt");

var rdd = lines.map(x => (x.split(" ")(0), (x.split(" ")(1))));

var P1 = rdd.groupByKey().collect;

//list of v with neightber vertex
var R1 = Iterable[String](); 
var X1 = Iterable[String]();

var r = bronKerboschAlgorithm(P1, R1, X1);

def bronKerboschAlgorithm(P:Array[(String, Iterable[String])], R:Iterable[String], X:Iterable[String]) : Iterable[String] = {
	var nP = P;
	var nR = R;
	var nX = X;

		
	if (nP.size == 0 && nX.size == 0) {
		println("******");
		println(nR.mkString("	"));
		println("******");
		return nR;
	}
	nP.foreach {
		case (v , nvs) => {
			nR = (nR ++ Iterable[String](v));
			// var nP = Array[(String, Iterable[String])]();
			var pP = Array[(String, Iterable[String])]();
			nvs.foreach{
				case (v2) => {
					nP.map{
						case (v3, nvs3) => {
							if (v2 == v3) {
								// println(v2==v3);
								pP = pP :+ (v3, nvs3);
							}
						}
					}

					// X.map{
					// 	case(x) => {
					// 		if (v2 == x) {
					// 			nX = (nX ++ Iterable[String](v2));
					// 		}	
					// 	}
					// }
				}
			}
			
			bronKerboschAlgorithm(pP, nR, nX);
			nP = nP.filter(
				_._1 != v
			);
			nX = (nX ++ Iterable[String](v));
			// // println(P1.size);
			// println(X1.size);
			// nP.mkString("\n");
			// bronKerboschAlgorithm()
			// println(v);
		}	
	}
	return nR;
};